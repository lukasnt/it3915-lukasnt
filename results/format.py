import sys
import pandas as pd

if __name__ == "__main__":
    for arg in sys.argv[1:]:
        print(arg)
        df = pd.read_csv(arg)
        df = df.drop(["sample"], axis=1)
        df = df.sort_values(["threadCount"])
        df["maxMemoryUsage"] = df["maxMemoryUsage"].apply(lambda x: int(x / (1024*1024)))
        df = df.to_csv(index=False)
        print(df)