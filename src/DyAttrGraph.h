#ifndef DYATTRGRAPH_H
#define DYATTRGRAPH_H

#include "AllocationMetrics.h"
#include "AttrGraph.h"
#include "CorePattern.h"

class TrendGraph;

class DyAttrGraph {
private:
    int attrCount;
    int timesteps;

public:
    std::map<int, std::unique_ptr<AttrGraph>> graphMap;

    DyAttrGraph(int attrCount, int timesteps);

    explicit DyAttrGraph(std::map<int, std::unique_ptr<AttrGraph>> graphMap);

    std::unique_ptr<TrendGraph> createTrendAg();

    std::set<int> getCompleteVIdSet();

    int getAttrCount() const;

    int getTimesteps() const;

    int calEdgeNumbers();

    int calVertexNumbers();

    void generateRandom(int vCount, int edgePerVCount, int attrValueRange, int attrValueChange, int neighborChange);

    void printGraphs();
};

class TrendGraph : public DyAttrGraph {

public:
    using DyAttrGraph::DyAttrGraph;

    void setAttrValue(int t, int vId, int trendAttr);

    void plantCorePatterns(const std::vector<std::unique_ptr<CorePattern>> *patterns, float minSup, float minLift, float minConf);
};

#endif