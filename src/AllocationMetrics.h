#ifndef ALLOCATIONMETRIX_H
#define ALLOCATIONMETRIX_H

#include <memory>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <iostream>
#include <atomic>

struct AllocationMetrics {
    std::atomic<uint64_t> totalAllocated;
    std::atomic<uint64_t> totalFreed;

    uint64_t currentUsage() {
        return totalAllocated - totalFreed;
    }
};

extern AllocationMetrics s_allocationMetrics;

/*
template<typename T> struct STLAllocator
{
    typedef T value_type;
    STLAllocator() = default;

    T* allocate(std::size_t n)
    {
        //Code that runs every allocation
        //s_allocationMetrics.totalAllocated += n;
        //std::cout << s_allocationMetrics.totalAllocated << std::endl;
        return std::allocator<T>{}.allocate(n);
    }

    void deallocate(T* mem, std::size_t n)
    {
        //Code that runs every deallocation
        //s_allocationMetrics.totalFreed += n;
        std::allocator<T>{}.deallocate(mem, n);
    }

};

namespace std {
    template <class T, class U> bool operator==(const STLAllocator<T>&, const STLAllocator<U>&) { return true; }
    template <class T, class U> bool operator!=(const STLAllocator<T>&, const STLAllocator<U>&) { return false; }
    template<typename T, typename Alloc = STLAllocator<T>> using tracked_vector = std::vector<T, Alloc>;
    template<typename T, typename Alloc = STLAllocator<T>> using tracked_set = std::set<T, std::less<T>, Alloc>;
    template<typename T, typename U, typename Alloc = STLAllocator<pair<const T, U>>> using tracked_map = std::map<T, U, std::less<T>, Alloc>;
    template<typename T, typename U, typename Alloc = STLAllocator<pair<const T, U>>> using tracked_unordered_map = std::unordered_map<T, U, std::less<T>, Alloc>;
}

#define vector tracked_vector
#define set tracked_set
#define map tracked_map
#define unordered_map tracked_unordered_map
*/
#endif
