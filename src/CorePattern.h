#ifndef COREPATTERN_H
#define COREPATTERN_H

#include "AllocationMetrics.h"
#include <string>

class CorePattern {
public:
    int size;
    int childAttr;
    std::vector<int> parentAttrs;

    explicit CorePattern(int attr);

    void addParentAttr(int attr);

    int getChildAttr() const;

    int getLatestParentAttr();

    int getSize() const;

    std::unique_ptr<CorePattern> clone();

    std::string to_string();
};
#endif