#ifndef VERTEX_H
#define VERTEX_H

#include "AllocationMetrics.h"
#include <memory>

class Vertex {
private:
    int id;
    int attrCount;
public:
    std::unique_ptr<int[]> attrValues;

    Vertex(int id, int attrCount);

    int getId() const;

    int getAttrCount() const;
};

#endif