#ifndef INVATTRMAP_H
#define INVATTRMAP_H

#include "AllocationMetrics.h"
#include "DyAttrGraph.h"

class InvAttrMap {
private:
    int attrCount;
    std::unique_ptr<std::map<int, std::set<int>>[]> data;
    std::unique_ptr<int[]> attrSups;

public:

    explicit InvAttrMap(int attrCount);

    void initFromDyAttrGraph(DyAttrGraph *dyAttrGraph);

    int getAttrCount() const;

    int getSupCount(int attr, int time);

    int getSupCount(int attr);

    std::set<int> *getSupSet(int attr, int time);

    std::map<int, std::set<int>> *getAttrMap(int attr);

    void print();
};

#endif