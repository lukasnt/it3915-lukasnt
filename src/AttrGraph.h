#ifndef ATTRGRAPH_H
#define ATTRGRAPH_H

#include "AllocationMetrics.h"
#include <memory>
#include "Vertex.h"

class AttrGraph {
private:
    std::map<int, std::unique_ptr<Vertex>> vMap;
    std::map<int, std::set<int>> edgesMap;

public:

    AttrGraph(std::map<int, std::unique_ptr<Vertex>> vMap, std::map<int, std::set<int>> edgesMap);

    int *getAttrValues(int vId);

    int getAttrValue(int vId, int attr);

    std::set<int> *getNeighbors(int vId);

    std::map<int, std::unique_ptr<Vertex>> *getVMap();

    void print();
};

#endif