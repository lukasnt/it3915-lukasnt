#include "Experiment.h"
#include "InvAttrMap.h"
#include "AERMiner.h"
#include <iostream>
#include <fstream>
#include <random>
#include <algorithm>



Experiment::Experiment(std::unique_ptr<TrendGraph> trendAg, float minSup, float minLift, float minConf) {
    auto invAttrMap = std::make_unique<InvAttrMap>(trendAg->getAttrCount());
    invAttrMap->initFromDyAttrGraph(trendAg.get());
    AerMiner = std::make_unique<AERMiner>(std::move(trendAg), std::move(invAttrMap), minSup, minConf, minLift);
}

void Experiment::run(int samples, std::vector<int> threadCounts) {
    std::cout << "Memory usage before run: " << s_allocationMetrics.currentUsage() / (1024 * 1024) << " MB" << std::endl;
    for (int s = 0; s < samples; s++) {
        std::cout << "Start experiment for sample " << s << ":" << std::endl;
        std::shuffle(threadCounts.begin(), threadCounts.end(), std::default_random_engine());
        for (int tCount : threadCounts) {
            std::cout << "Thread count: " << tCount << std::endl;
            auto start = std::chrono::high_resolution_clock::now();
            s_allocationMetrics.totalAllocated = 0;

            if (tCount == 1)
                AerMiner->runSequentialAlgo();
            else
                AerMiner->runMultiThreadedAlgo(tCount);

            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
            std::cout << "duration: " << duration.count() << " milliseconds" << std::endl;
            std::cout << "Maximum memory usage: " << AerMiner->getMaxMemoryUsage() / (1024 * 1024) << " MB" << std::endl;

            Experiment::measurements.emplace_back(Measurement {
                .sample =  s,
                .threadCount =  tCount,
                .runtimeMilliseconds =  (int) duration.count(),
                .maxMemoryUsageBytes =  AerMiner->getMaxMemoryUsage()
            });
        }
        std::cout << std::endl;
    }
    s_allocationMetrics.totalAllocated = 0;
}

void Experiment::saveMeasurements(std::string filename) {
    std::ofstream fw(filename, std::ofstream::out);
    if (fw.is_open()) {
        fw << "sample,threadCount,runTime,maxMemoryUsage" << "\n";
        for (auto m : Experiment::measurements) {
            fw << m.sample << "," << m.threadCount << "," << m.runtimeMilliseconds << "," << m.maxMemoryUsageBytes << "\n";
        }
        fw.close();
    }
}

uint64_t Experiment::getMemoryUsage() {
    return s_allocationMetrics.currentUsage();
}