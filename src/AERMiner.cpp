#include "AERMiner.h"
#include "Experiment.h"
#include <iostream>
#include <thread>
#include <future>
#include <cmath>
#include <algorithm>

AERMiner::AERMiner(std::unique_ptr<DyAttrGraph> trendAg, std::unique_ptr<InvAttrMap> invAttrMap, float minSup, float minConf, float minLift) :
        trendAg(std::move(trendAg)),
        invAttrMap(std::move(invAttrMap)),
        minSup(minSup),
        minConf(minConf),
        minLift(minLift),
        minSupRelative((int) (minSup * (float) trendAg->calEdgeNumbers()))
        {
            maxThreadCount = 16;
            expectConfMap = calExpectedConfidence();
            minSupRelativeMap = calMinSupRelativeMap();
            startMemory = Experiment::getMemoryUsage();
        }


std::map<int, float> AERMiner::calExpectedConfidence() {
    std::map<int, float> expConfMap;
    for (int attr = 0; attr < invAttrMap->getAttrCount(); attr++) {
        float conf = (float) invAttrMap->getSupCount(attr) / (float) trendAg->calVertexNumbers();
        expConfMap[attr] = conf;
    }
    return expConfMap;
}

std::map<int, int> AERMiner::calMinSupRelativeMap() {
    std::map<int, int> result;
    int avgEdgeCount = std::ceil(trendAg->calEdgeNumbers() / trendAg->calVertexNumbers());
    result[1] = (int) ((float) trendAg->calVertexNumbers() * minSup);
    for (int k = 2; k <= avgEdgeCount + 1; k++) {
        result[k] = result[k - 1] * (avgEdgeCount - k + 3);
    }
    return result;
}

std::unique_ptr<CorePatternMap> AERMiner::initCorePatternMap() {
    auto corePatternMap = std::make_unique<CorePatternMap>();
    for (int attr = 0; attr < invAttrMap->getAttrCount(); attr++) {
        if (attr % 3 != 1) {
            auto corePattern = std::make_unique<CorePattern>(attr);
            auto instanceSet = std::set<std::unique_ptr<Instance>>();
            for (auto &it: *invAttrMap->getAttrMap(attr)) {
                for (int vId: it.second) {
                    auto instance = std::make_unique<Instance>(it.first, vId);
                    instanceSet.insert(std::move(instance));
                }
            }
            corePatternMap->addPatternInstances(std::move(corePattern), std::move(instanceSet));
        }
    }
    return std::move(corePatternMap);
}

std::pair<float, float> AERMiner::calNewPatternLift(int attr, CorePattern* pattern, std::set<std::unique_ptr<Instance>>* instances) {
    int count = 0;
    int sum = 0;
    for (auto &instance : *instances) {
        int prevT = instance->timestamp - 1;
        if (prevT < 1) continue;

        auto valid_neighbors = *trendAg->graphMap.at(prevT)->getNeighbors(instance->vId);
        for (int pId : instance->parentIds) valid_neighbors.erase(pId);
        for (auto nId : valid_neighbors) {
            sum++;
            if (trendAg->graphMap.at(prevT)->getAttrValue(nId, attr)) {
                count++;
            }
        }
    }
    float conf = sum > 0 ? (float) count / (float) sum : 0;
    float lift = (float) conf / expectConfMap[attr];
    // std::cout << "count: " << count << ", sum: " << sum << std::endl;
    // std::cout << "conf: " << conf << ", lift: " << lift << ", expectConf: " << expectConfMap[attr] << std::endl;

    // two kind of expect confidence, when size is equal 1, its confidence is
    // probability
    // size > 1: the expect confidence is based on child attribute

    return {lift, conf};
}

std::unique_ptr<CorePatternMap> AERMiner::extendCorePatterns(CorePatternMap* corePatternMap) {
    auto newCorePatternMap = std::make_unique<CorePatternMap>();
    for (auto &pattern: *corePatternMap->getKeyMap()) {

        int lastAttr = pattern.second->getLatestParentAttr();
        for (int attr = lastAttr; attr < invAttrMap->getAttrCount(); attr++) {
            if (attr % 3 != 1 && attr > lastAttr) {
                auto newPattern = std::move(pattern.second->clone());
                newPattern->addParentAttr(attr);

                auto newInstances = std::move(AERMiner::extendInstances(attr, corePatternMap->getInstances(pattern.first)));
                auto sup = newInstances.size();
                //std::cout << "sup: " << sup << ", minsuprelative: " << minSupRelativeMap[newPattern->getSize()] << std::endl;
                if (sup < minSupRelativeMap[newPattern->getSize()]) continue;

                auto liftConf = calNewPatternLift(attr, newPattern.get(), &newInstances);
                //std::cout << "lift: " << liftConf.first << ", conf: " << liftConf.second << std::endl;
                if (liftConf.first < minLift) continue;
                patternConfMap[newPattern->to_string()] = liftConf.second;

                newCorePatternMap->addPatternInstances(std::move(newPattern), std::move(newInstances));
                //std::cout << "support count " << sup << std::endl;
            }
        }
    }
    return std::move(newCorePatternMap);
}


std::set<std::unique_ptr<Instance>> AERMiner::extendInstances(int attr, std::set<std::unique_ptr<Instance>>* instances) {
    std::set<std::unique_ptr<Instance>> newInstances;
    //std::cout << "instance size: " << instances->size() << std::endl;
    for (auto &instance : *instances) {
        int prevT = instance->timestamp - 1;
        if (prevT < 1) continue;

        auto valid_neighbors = *trendAg->graphMap.at(prevT)->getNeighbors(instance->vId);
        for (int pId : instance->parentIds) valid_neighbors.erase(pId);
        for (auto nId : valid_neighbors) {
            if (trendAg->graphMap.at(prevT)->getAttrValue(nId, attr)) {
                auto newInstance = std::move(instance->clone());
                newInstance->addParentId(nId);
                newInstances.insert(std::move(newInstance));
            }
        }
    }
    return std::move(newInstances);
}

std::vector<std::unique_ptr<CorePatternMap>> AERMiner::bfsSearch(std::unique_ptr<CorePatternMap> initCorePatternMap) {
    //std::cout << "start bfs" << std::endl;
    std::vector<std::unique_ptr<CorePatternMap>> patterns;
    auto latestCorePatternMap = std::move(initCorePatternMap);
    int k = 0;
    while (latestCorePatternMap->size()) {
        std::cout << "k: " << k << std::endl;

        patterns.push_back(std::move(latestCorePatternMap));
        latestCorePatternMap = std::move(extendCorePatterns(patterns.back().get()));

        k++;
    }
    std::cout << "k: " << k << std::endl;
    return std::move(patterns);
}

std::unique_ptr<CorePatternMap> AERMiner::filterCorePatterns(std::unique_ptr<CorePatternMap> corePatternMap) {
    auto filteredCorePatternMap = std::make_unique<CorePatternMap>();
    for (auto &pattern : *corePatternMap->getKeyMap()) {
        if (patternConfMap[pattern.first] >= minConf) {
            filteredCorePatternMap->addPatternInstances(std::move(pattern.second), std::move(corePatternMap->getInstancesMap()->at(pattern.first)));
        }
    }
    corePatternMap->releaseAllInstances();
    return std::move(filteredCorePatternMap);
}

std::unique_ptr<CorePatternMap> AERMiner::mergeCorePatterns(std::unique_ptr<CorePatternMap> corePatternMap) {
    return std::move(corePatternMap);
}

std::pair<std::unique_ptr<CorePatternMap>, std::unique_ptr<CorePatternMap>> AERMiner::multiThreadedExtendCorePatterns(std::unique_ptr<CorePatternMap> corePatternMap) {
    auto extendCorePatternFunction = [](CorePatternMap* mapSplit, AERMiner* miner, std::promise<std::unique_ptr<CorePatternMap>> promise) {
        auto patternMap = miner->extendCorePatterns(mapSplit);
        promise.set_value(std::move(patternMap));
    };

    std::vector<std::thread> threads;
    std::vector<std::future<std::unique_ptr<CorePatternMap>>> futures;
    auto currentCorePatternMap = std::move(corePatternMap);
    auto mapSplits = std::move(currentCorePatternMap->splitMap(AERMiner::maxThreadCount));
    for (auto &split : mapSplits) {
        std::promise<std::unique_ptr<CorePatternMap>> promise;
        futures.emplace_back(promise.get_future());
        threads.emplace_back(std::thread(extendCorePatternFunction, split.get(), this, std::move(promise)));
    }
    for (auto &t : threads) if (t.joinable()) t.join();

    auto nextCorePatternMap = std::make_unique<CorePatternMap>();
    auto nextMapSplits = std::vector<std::unique_ptr<CorePatternMap>>();
    nextMapSplits.reserve(futures.size());
    for (auto &f : futures) nextMapSplits.emplace_back(std::move(f.get()));
    nextCorePatternMap->mergeMaps(std::move(nextMapSplits));
    currentCorePatternMap->mergeMaps(std::move(mapSplits));

    return {std::move(currentCorePatternMap), std::move(nextCorePatternMap)};
}

std::vector<std::unique_ptr<CorePatternMap>> AERMiner::multiThreadedBfsSearch(std::unique_ptr<CorePatternMap> initCorePatternMap) {
    std::vector<std::unique_ptr<CorePatternMap>> patterns;
    std::vector<std::thread> concurrentFilterMergeThreads;
    std::vector<std::future<std::unique_ptr<CorePatternMap>>> concurrentFilterMergeFutures;
    auto currentCorePatternMap = std::move(initCorePatternMap);

    int k = 0;
    AERMiner::memoryCheckpointMap[k] = Experiment::getMemoryUsage() - AERMiner::startMemory;
    while (currentCorePatternMap->size()) {
        std::cout << "k: " << k << std::endl;

        auto extendCorePatternsResult = multiThreadedExtendCorePatterns(std::move(currentCorePatternMap));
        currentCorePatternMap = std::move(extendCorePatternsResult.second);

        auto concurrentFilterMergeResult = concurrentFilterMerge(std::move(extendCorePatternsResult.first));
        concurrentFilterMergeThreads.emplace_back(std::move(concurrentFilterMergeResult.first));
        concurrentFilterMergeFutures.emplace_back(std::move(concurrentFilterMergeResult.second));

        k++;
        AERMiner::memoryCheckpointMap[k] = Experiment::getMemoryUsage() - AERMiner::startMemory - AERMiner::getTotalMemoryUsed();
    }
    std::cout << "k: " << k << std::endl;

    patterns.reserve(concurrentFilterMergeFutures.size());
    for (auto &thread : concurrentFilterMergeThreads) if (thread.joinable()) thread.join();
    for (auto &future : concurrentFilterMergeFutures) patterns.emplace_back(std::move(future.get()));
    return std::move(patterns);
}

std::pair<std::thread, std::future<std::unique_ptr<CorePatternMap>>> AERMiner::concurrentFilterMerge(std::unique_ptr<CorePatternMap> patternMap) {
    auto filterCorePatternsFunction = [](std::unique_ptr<CorePatternMap> corePatternMap, AERMiner* miner, std::promise<std::unique_ptr<CorePatternMap>> promise) {
        auto result = miner->filterCorePatterns(std::move(corePatternMap));
        result->releaseAllInstances();
        promise.set_value(std::move(result));
    };

    auto promise = std::promise<std::unique_ptr<CorePatternMap>>();
    auto future = promise.get_future();
    auto thread = std::thread(filterCorePatternsFunction, std::move(patternMap), this, std::move(promise));

    return {std::move(thread), std::move(future)};
}

void AERMiner::runSequentialAlgo() {
    AERMiner::resetMemoryStats();
    auto initCorePattern = AERMiner::initCorePatternMap();
    auto patterns = AERMiner::bfsSearch(std::move(initCorePattern));
    for (auto &patternMap : patterns) {
        auto filteredPatternMap = filterCorePatterns(std::move(patternMap));
        filteredPatternMap->printPatterns();
    }
    AERMiner::memoryCheckpointMap[0] = Experiment::getMemoryUsage() - AERMiner::startMemory;
}

void AERMiner::runMultiThreadedAlgo(int maxThreadCount) {
    AERMiner::resetMemoryStats();
    AERMiner::maxThreadCount = maxThreadCount;
    auto initCorePattern = AERMiner::initCorePatternMap();
    auto patterns = AERMiner::multiThreadedBfsSearch(std::move(initCorePattern));
    for (auto &patternMap : patterns) patternMap->printPatterns();
}

void AERMiner::resetMemoryStats() {
    AERMiner::memoryCheckpointMap.clear();
    AERMiner::startMemory = Experiment::getMemoryUsage();
}

uint64_t AERMiner::getMaxMemoryUsage() {
    uint64_t maxMemory = 0;
    for (auto mIt : memoryCheckpointMap) {
        if (mIt.second > maxMemory) maxMemory = mIt.second;
    }
    return maxMemory;
}

uint64_t AERMiner::getTotalMemoryUsed() {
    uint64_t total = 0;
    for (auto mIt : memoryCheckpointMap) {
        total += mIt.second;
    }
    return total;
}
