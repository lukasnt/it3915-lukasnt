#ifndef INSTANCE
#define INSTANCE

#include "AllocationMetrics.h"
#include <memory>

class Instance {
public:
    int timestamp;
    int vId;
    std::vector<int> parentIds;

    Instance(int timestamp, int vId);

    void addParentId(int vId);

    std::vector<int>* getParentIds();

    std::unique_ptr<Instance> clone();
};
#endif