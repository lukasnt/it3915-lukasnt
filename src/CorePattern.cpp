#include "CorePattern.h"

CorePattern::CorePattern(int attrib) {
    childAttr = attrib;
    size = 1;
}

void CorePattern::addParentAttr(int attr) {
    parentAttrs.push_back(attr);

    size++;
}
int CorePattern::getChildAttr() const {
    return childAttr;
}

int CorePattern::getSize() const {
    return size;
}

std::unique_ptr<CorePattern> CorePattern::clone() {
    auto newCorePattern = std::make_unique<CorePattern>(CorePattern::childAttr);
    newCorePattern->parentAttrs = CorePattern::parentAttrs;
    newCorePattern->size = CorePattern::size;
    return std::move(newCorePattern);
}

std::string CorePattern::to_string() {
    std::string result;
    for (int attr : CorePattern::parentAttrs) {
        result += std::to_string(attr) + ",";
    }
    return result + "->" + std::to_string(childAttr);
}

int CorePattern::getLatestParentAttr() {
    if (!parentAttrs.empty())
        return parentAttrs.back();
    else return 0;
}


