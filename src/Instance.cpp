#include "Instance.h"

Instance::Instance(int timestamp, int vId) :
        timestamp(timestamp),
        vId(vId) {}

void Instance::addParentId(int vId) {
    parentIds.push_back(vId);
}

std::vector<int> *Instance::getParentIds() {
    return &parentIds;
}

std::unique_ptr<Instance> Instance::clone() {
    auto newInstance = std::make_unique<Instance>(Instance::timestamp, Instance::vId);
    newInstance->parentIds = Instance::parentIds;
    return std::move(newInstance);
}
