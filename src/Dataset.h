#ifndef DATASET_H
#define DATASET_H

#include <memory>
#include "DyAttrGraph.h"

class Dataset {
public:
    static std::unique_ptr<TrendGraph> read(std::string filepath);

};


#endif
