#ifndef COREPATTERNMAP_H
#define COREPATTERNMAP_H

#include "AllocationMetrics.h"
#include "CorePattern.h"
#include "Instance.h"
#include <string>

class CorePatternMap {
private:
    std::map<std::string, std::set<std::unique_ptr<Instance>>> instancesData;
    std::map<std::string, std::unique_ptr<CorePattern>> corePatternData;
public:
    CorePatternMap();

    void addPatternInstances(std::unique_ptr<CorePattern> pattern, std::set<std::unique_ptr<Instance>> instances);

    void removePatternInstances(const std::string& patternKey);

    std::map<std::string, std::set<std::unique_ptr<Instance>>>* getInstancesMap();

    std::map<std::string, std::unique_ptr<CorePattern>>* getKeyMap();

    CorePattern* getCorePattern(const std::string& patternKey);

    std::set<std::unique_ptr<Instance>>* getInstances(const std::string& patternKey);

    std::vector<std::unique_ptr<CorePatternMap>> splitMap(int partitions);

    void mergeMaps(std::vector<std::unique_ptr<CorePatternMap>> mapPartitions);

    void releaseAllInstances();

    int size();

    void printPatterns();
};

#endif
