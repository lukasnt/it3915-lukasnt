#include <memory>
#include <vector>
#include <iostream>
#include "AllocationMetrics.h"
#include "Experiment.h"
#include "Dataset.h"

void* operator new(size_t size) {
    s_allocationMetrics.totalAllocated += size;
    return malloc(size);
};

void operator delete(void* memory, size_t size) {
    s_allocationMetrics.totalFreed += size;
    free(memory);
};

int sampleSize = 10;
auto threadCounts = {1, 2, 4, 8, 16, 32};

void runMediumSizeMediumPruningTest() {
    int attrCount = 8;
    int timesteps = 15;
    auto dyAttrGraph = std::make_unique<DyAttrGraph>(attrCount, timesteps);

    int vCount = 150;
    int edgesPerVertex = 5;
    int attrValueRange = 10;
    int attrChange = 2;
    int neighborChange = 0;
    dyAttrGraph->generateRandom(vCount, edgesPerVertex, attrValueRange, attrChange, neighborChange);
    auto trendAg = dyAttrGraph->createTrendAg();

    auto testCorePatterns = std::vector<std::unique_ptr<CorePattern>>();
    auto p1 = std::make_unique<CorePattern>(3);
    p1->addParentAttr(2);
    p1->addParentAttr(5);
    p1->addParentAttr(6);
    auto p2 = std::make_unique<CorePattern>(9);
    p2->addParentAttr(14);
    p2->addParentAttr(15);
    p2->addParentAttr(20);
    testCorePatterns.emplace_back(std::move(p2));
    trendAg->plantCorePatterns(&testCorePatterns, 0.10, 0, 0);

    float minSup = 0.1;
    float minConf = 0.5;
    float minLift = 1.00;
    auto experiment = std::make_unique<Experiment>(std::move(trendAg), minSup, minConf, minLift);
    std::cout << std::endl << std::endl;
    std::cout << "Starting Medium Size Medium Pruning Experiment" << std::endl;
    experiment->run(sampleSize, threadCounts);
    experiment->saveMeasurements("./medium_size_medium_pruning_measurements.csv");
}

void runLargeSizeHighPruningTest() {
    int attrCount = 8;
    int timesteps = 30;
    auto dyAttrGraph = std::make_unique<DyAttrGraph>(attrCount, timesteps);

    int vCount = 500;
    int edgesPerVertex = 5;
    int attrValueRange = 10;
    int attrChange = 2;
    int neighborChange = 0;
    dyAttrGraph->generateRandom(vCount, edgesPerVertex, attrValueRange, attrChange, neighborChange);
    auto trendAg = dyAttrGraph->createTrendAg();

    auto testCorePatterns = std::vector<std::unique_ptr<CorePattern>>();
    auto p1 = std::make_unique<CorePattern>(3);
    p1->addParentAttr(2);
    p1->addParentAttr(5);
    p1->addParentAttr(6);
    p1->addParentAttr(14);
    p1->addParentAttr(15);
    p1->addParentAttr(20);
    trendAg->plantCorePatterns(&testCorePatterns, 0.15, 0, 0);

    float minSup = 0.12;
    float minConf = 0.5;
    float minLift = 1.1;
    auto experiment = std::make_unique<Experiment>(std::move(trendAg), minSup, minConf, minLift);
    std::cout << std::endl << std::endl;
    std::cout << "Starting Large Size High Pruning Experiment" << std::endl;
    experiment->run(sampleSize, threadCounts);
    experiment->saveMeasurements("./large_size_high_pruning_measurements.csv");
}

void runUSFlightDataset() {
    auto trendAg = Dataset::read("./datasets/USFlight");

    float minSup = 0.365;
    float minConf = 0.5;
    float minLift = 1.6;
    auto experiment = std::make_unique<Experiment>(std::move(trendAg), minSup, minConf, minLift);
    std::cout << std::endl << std::endl;
    std::cout << "Starting USFlight Dataset Experiment" << std::endl;
    experiment->run(sampleSize, threadCounts);
    experiment->saveMeasurements("./usflight_dataset_measurements.csv");
}

void runDBLPDataset() {
    auto trendAg = Dataset::read("./datasets/DBLP");

    float minSup = 0.307;
    float minConf = 0.5;
    float minLift = 3.0;
    auto experiment = std::make_unique<Experiment>(std::move(trendAg), minSup, minConf, minLift);
    std::cout << std::endl << std::endl;
    std::cout << "Starting DBLP Dataset Experiment" << std::endl;
    experiment->run(sampleSize, threadCounts);
    experiment->saveMeasurements("./dblp_dataset_measurements.csv");
}

int main() {
    runUSFlightDataset();
    runDBLPDataset();
    runMediumSizeMediumPruningTest();
    runLargeSizeHighPruningTest();
}