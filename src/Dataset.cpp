#include "Dataset.h"
#include <fstream>
#include <string>
#include <iostream>
#include <memory>
#include <set>
#include <map>

std::vector<std::string> split(std::string str, std::string deli = " ") {
    std::vector<std::string> result;
    int start = 0;
    int end = str.find(deli);
    if (end == -1 )
        result.emplace_back(str.substr(start));
    else {
        while (end != -1) {
            result.emplace_back(str.substr(start, end - start));
            start = end + deli.size();
            end = str.find(deli, start);
        }
        result.emplace_back(str.substr(start));
    }
    return result;
}

std::unique_ptr<TrendGraph> Dataset::read(std::string filepath) {
    std::map<int, std::map<int, std::unique_ptr<Vertex>>> vertexMaps;
    std::fstream attributes(!filepath.empty() ? filepath + "/attributes.txt" : "./attributes.txt");
    if (attributes.is_open()) {
        std::string line;
        int tId = 0;
        int vId = 0;
        while (std::getline(attributes, line)) {
            std::vector<std::string> tokens = split(line);
            if (tokens.size() == 1) {
                tId = (int) std::stoi(tokens[0].substr(tokens[0].find("T") + 1));
                vertexMaps[tId] = std::map<int, std::unique_ptr<Vertex>>();
            } else if (!tokens.empty()) {
                vId = (int) std::stoi(tokens[0]);
                auto newVertex = std::make_unique<Vertex>(vId, tokens.size() - 1);
                for (int i = 1; i < tokens.size(); i++) {
                    newVertex->attrValues[i - 1] = (int) (std::stoi(tokens[i]) * 100000);
                }
                vertexMaps[tId][vId] = std::move(newVertex);
            }

        }
        attributes.close();
    }

    std::fstream graph(!filepath.empty() ? filepath + "/graph.txt" : "./graph.txt");
    std::map<int, std::unique_ptr<AttrGraph>> graphMap;
    std::map<int, std::map<int, std::set<int>>> edgesMap;
    if (graph.is_open()) {
        std::string line;
        int tId = -1;
        while(std::getline(graph, line)) {
            std::vector<std::string> tokens = split(line);
            int vId = 0;
            if (tokens.size() == 1 && tokens[0].find("T") != -1) {
                if (tId != -1) {
                    graphMap[tId] = std::move(std::make_unique<AttrGraph>(std::move(vertexMaps[tId]), std::move(edgesMap[tId])));
                }
                tId = (int) std::stoi(tokens[0].substr(tokens[0].find("T") + 1));
            } else if (!tokens.empty()) {
                if (tokens[0].find(" ") != -1)
                    tokens[0] = tokens[0].substr(tokens[0].find(" "));
                if (tokens[0].empty()) continue;
                vId = (int) std::stoi(tokens[0]);
                auto neighborSet = std::set<int>();
                for (int i = 1; i < tokens.size(); i++) {
                    if (!tokens[i].empty()) neighborSet.insert((int) (std::stoi(tokens[i])));
                }
                edgesMap[tId][vId] = std::move(neighborSet);
            }
        }
        graph.close();
    }

    auto dyAttrGraph = std::make_unique<DyAttrGraph>(std::move(graphMap));

    return std::move(dyAttrGraph->createTrendAg());
}
