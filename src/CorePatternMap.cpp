#include "CorePatternMap.h"
#include <iostream>
#include <cmath>

CorePatternMap::CorePatternMap() = default;

void CorePatternMap::addPatternInstances(std::unique_ptr<CorePattern> pattern, std::set<std::unique_ptr<Instance>> instances) {
    auto patternKey = pattern->to_string();
    corePatternData[patternKey] = std::move(pattern);
    instancesData[patternKey] = std::move(instances);
}

void CorePatternMap::removePatternInstances(const std::string& patternKey) {
    corePatternData.erase(patternKey);
    instancesData.erase(patternKey);
}

std::set<std::unique_ptr<Instance>> *CorePatternMap::getInstances(const std::string& patternKey) {
    return &instancesData.at(patternKey);
}

std::map<std::string, std::set<std::unique_ptr<Instance>>> *CorePatternMap::getInstancesMap() {
    return &instancesData;
}

std::map<std::string, std::unique_ptr<CorePattern>> *CorePatternMap::getKeyMap() {
    return &corePatternData;
}

CorePattern *CorePatternMap::getCorePattern(const std::string& patternKey) {
    return corePatternData.at(patternKey).get();
}

std::vector<std::unique_ptr<CorePatternMap>> CorePatternMap::splitMap(int partitions) {
    std::vector<std::unique_ptr<CorePatternMap>> mapPartitions;
    int partitionSize = std::ceil(CorePatternMap::size() / partitions);

    int counter = 0;
    auto currentMapPartition = std::make_unique<CorePatternMap>();
    for (auto &pattern : *CorePatternMap::getKeyMap()) {
        if (counter >= partitionSize) {
            mapPartitions.push_back(std::move(currentMapPartition));
            currentMapPartition = std::make_unique<CorePatternMap>();
            counter = 0;
        }
        currentMapPartition->addPatternInstances(std::move(pattern.second), std::move(instancesData.at(pattern.first)));
        counter++;
    }
    mapPartitions.push_back(std::move(currentMapPartition));
    return std::move(mapPartitions);
}

void CorePatternMap::mergeMaps(std::vector<std::unique_ptr<CorePatternMap>> mapPartitions) {
    for (auto &map : mapPartitions) {
        for (auto &pattern : *map->getKeyMap()) {
            CorePatternMap::addPatternInstances(std::move(pattern.second), std::move(*map->getInstances(pattern.first)));
        }
    }
}

void CorePatternMap::releaseAllInstances() {
    /*
    //std::cout << instancesData.size() << std::endl;
    for (auto &instanceSetIt : instancesData) {
        //std::cout << 123 << std::endl;
        for (auto &instance : instanceSetIt.second) {
            auto free = (*instance).parentIds.size() * sizeof(int) + sizeof(Instance);
            //std::cout << free << std::endl;
            s_allocationMetrics.totalFreed += free;
        }
    }
     */
    instancesData.clear();
}

int CorePatternMap::size() {
    return corePatternData.size();
}

void CorePatternMap::printPatterns() {
    for(auto &it : corePatternData) {
        std::cout << it.second->to_string() << std::endl;
    }
}