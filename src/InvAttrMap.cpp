#include <memory>
#include <iostream>
#include <map>
#include <set>

#include "InvAttrMap.h"
#include "DyAttrGraph.h"

InvAttrMap::InvAttrMap(int attrCount) {
    InvAttrMap::attrCount = attrCount;
    InvAttrMap::data = std::make_unique<std::map<int, std::set<int>>[]>(attrCount);
    InvAttrMap::attrSups = std::make_unique<int[]>(attrCount);
}

void InvAttrMap::initFromDyAttrGraph(DyAttrGraph *dyAttrGraph) {
    std::cout << dyAttrGraph->getTimesteps() << ", " << dyAttrGraph->getAttrCount() << std::endl;
    for (auto gIt = dyAttrGraph->graphMap.begin(); gIt != dyAttrGraph->graphMap.end(); gIt++) {
        for (auto vIt = gIt->second->getVMap()->begin();
             vIt != gIt->second->getVMap()->end();
             vIt++) {
            for (int attr = 0; attr < dyAttrGraph->getAttrCount(); attr++) {
                if (vIt->second->attrValues[attr]) {
                    data[attr][gIt->first].insert(vIt->first);
                    attrSups[attr]++;
                }
            }
        }
    }
}

int InvAttrMap::getAttrCount() const {
    return attrCount;
}

int InvAttrMap::getSupCount(int attr, int time) {
    return data[attr][time].size();
}

int InvAttrMap::getSupCount(int attr) {
    return attrSups[attr];
}

std::map<int, std::set<int>> *InvAttrMap::getAttrMap(int attr) {
    return &data[attr];
}

std::set<int> *InvAttrMap::getSupSet(int attr, int time) {
    return &data[attr][time];
}

void InvAttrMap::print() {
    for (int attr = 0; attr < attrCount; attr++) {
        std::cout << "attrSup " << attr << ": " << attrSups[attr] << std::endl;
        for (const auto& timeMap: InvAttrMap::data[attr]) {
            std::cout << "attr " << attr << ", time " << timeMap.first << " size: "
                      << InvAttrMap::getSupCount(attr, timeMap.first)
                      << std::endl;
        }
    }
}

