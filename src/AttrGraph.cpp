#include "AttrGraph.h"
#include "Vertex.h"
#include <memory>
#include <iostream>

AttrGraph::AttrGraph(std::map<int, std::unique_ptr<Vertex>> vMap, std::map<int, std::set<int>> edgesMap) :
        vMap(std::move(vMap)),
        edgesMap(std::move(edgesMap)) {}


int *AttrGraph::getAttrValues(int vId) {
    return vMap.at(vId)->attrValues.get();
}

int AttrGraph::getAttrValue(int vId, int attr) {
    return vMap.at(vId)->attrValues[attr];
}

std::set<int> *AttrGraph::getNeighbors(int vId) {
    return &edgesMap[vId];
}

std::map<int, std::unique_ptr<Vertex>> *AttrGraph::getVMap() {
    return &vMap;
}

void AttrGraph::print() {
    for (auto &it : vMap) {
        std::cout << "Attribute Values: ";
        for (int attr = 0; attr < it.second->getAttrCount(); attr++) {
            std::cout << it.second->attrValues[attr] << ",";
        }
        std::cout << " Neighbors: ";
        for (auto nit : edgesMap[it.first]) {
            std::cout << nit << ",";
        }
        std::cout << std::endl;
    }
}