#include <memory>

#include "Vertex.h"

Vertex::Vertex(int vId, int attrCount) {
    id = vId;
    attrValues = std::make_unique<int[]>(attrCount);
    Vertex::attrCount = attrCount;
}

int Vertex::getId() const {
    return id;
}

int Vertex::getAttrCount() const {
    return attrCount;
}