#ifndef AERMINER_H
#define AERMINER_H

#include "AllocationMetrics.h"
#include "DyAttrGraph.h"
#include "InvAttrMap.h"
#include "CorePatternMap.h"
#include "Instance.h"
#include <future>
#include <thread>

class AERMiner {
private:
    float minSup;
    float minConf;
    float minLift;

    int minSupRelative;
    int maxThreadCount;

    std::unique_ptr<DyAttrGraph> trendAg;
    std::unique_ptr<InvAttrMap> invAttrMap;
    std::map<int, int> minSupRelativeMap;
    std::map<int, float> expectConfMap;
    std::map<int, std::map<int, float>> expectConfBasedAttrMap;
    std::map<std::string, float> patternConfMap;

    std::map<int, uint64_t> memoryCheckpointMap;
    uint64_t startMemory;

public:
    AERMiner(std::unique_ptr<DyAttrGraph> trendAg, std::unique_ptr<InvAttrMap> invAttrMap, float minSup, float minConf, float minLift);

    std::map<int, int> calMinSupRelativeMap();

    std::map<int, float> calExpectedConfidence();

    std::unique_ptr<CorePatternMap> initCorePatternMap();

    std::pair<float, float> calNewPatternLift(int attr, CorePattern* pattern, std::set<std::unique_ptr<Instance>>* instances);

    std::unique_ptr<CorePatternMap> extendCorePatterns(CorePatternMap* corePatternMap);

    std::set<std::unique_ptr<Instance>> extendInstances(int attr, std::set<std::unique_ptr<Instance>>* instances);

    std::vector<std::unique_ptr<CorePatternMap>> bfsSearch(std::unique_ptr<CorePatternMap> initCorePatternMap);

    std::unique_ptr<CorePatternMap> filterCorePatterns(std::unique_ptr<CorePatternMap> corePatternMap);

    std::unique_ptr<CorePatternMap> mergeCorePatterns(std::unique_ptr<CorePatternMap> corePatternMap);

    std::unique_ptr<CorePatternMap> multiThreadedExtendInstances(std::unique_ptr<CorePatternMap> corePatternMap);

    std::pair<std::unique_ptr<CorePatternMap>, std::unique_ptr<CorePatternMap>> multiThreadedExtendCorePatterns(std::unique_ptr<CorePatternMap> corePatternMap);

    std::vector<std::unique_ptr<CorePatternMap>> multiThreadedBfsSearch(std::unique_ptr<CorePatternMap> initCorePatternMap);

    std::pair<std::thread, std::future<std::unique_ptr<CorePatternMap>>> concurrentFilterMerge(std::unique_ptr<CorePatternMap> patternMap);

    uint64_t getMaxMemoryUsage();

    uint64_t getTotalMemoryUsed();

    void resetMemoryStats();

    void runSequentialAlgo();

    void runMultiThreadedAlgo(int maxThreadCount);
};

#endif
