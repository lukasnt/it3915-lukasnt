#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "DyAttrGraph.h"
#include "CorePatternMap.h"
#include "AERMiner.h"

struct Measurement{
    int sample;
    int threadCount;
    int runtimeMilliseconds;
    uint64_t maxMemoryUsageBytes;
};

class Experiment {
private:
    std::vector<Measurement> measurements;
    std::unique_ptr<AERMiner> AerMiner;

public:
    Experiment(std::unique_ptr<TrendGraph> trendAg, float minSup, float minLift, float minConf);

    void run(int samples, std::vector<int> threadCounts);

    void saveMeasurements(std::string filename);

    static uint64_t getMemoryUsage();
};


#endif
