#include <cstdlib>
#include <memory>
#include <iostream>
#include <tuple>
#include "DyAttrGraph.h"
#include "InvAttrMap.h"

DyAttrGraph::DyAttrGraph(int attrCount, int timesteps) {
    DyAttrGraph::attrCount = attrCount;
    DyAttrGraph::timesteps = timesteps;
}

DyAttrGraph::DyAttrGraph(std::map<int, std::unique_ptr<AttrGraph>> graphMap)
        : graphMap(std::move(graphMap)) {
    DyAttrGraph::attrCount = DyAttrGraph::graphMap.at(1)->getVMap()->begin()->second->getAttrCount();
    DyAttrGraph::timesteps = DyAttrGraph::graphMap.size();
}

void DyAttrGraph::generateRandom(int vCount, int maxEdgePerVertex, int attrValueRange, int attrValueChange,
                                 int neighborChange) {
    // Create AttrGraph for time=0 which will be slightly changed each timestep
    std::map<int, std::unique_ptr<Vertex>> initVMap;
    std::map<int, std::set<int>> initEdgesMap;
    int edges = 0;
    for (int v = 0; v < vCount; v++) {
        // Create vertex with random attribute values
        auto vertex = std::make_unique<Vertex>(v, attrCount);
        for (int attr = 0; attr < attrCount; attr++) {
            int attrValue = std::rand() % attrValueRange;
            vertex->attrValues[attr] = attrValue;
        }

        // Create neighborhood set to this vertex
        while (initEdgesMap[v].size() < maxEdgePerVertex && edges < maxEdgePerVertex * vCount) {
            int neighborV = std::rand() % vCount;
            if (initEdgesMap[neighborV].size() < maxEdgePerVertex) {
                initEdgesMap[v].insert(neighborV);
                initEdgesMap[neighborV].insert(v);
                edges++;
            }
        }
        initVMap[v] = std::move(vertex);
    }
    graphMap[0] = std::move(std::make_unique<AttrGraph>(std::move(initVMap), std::move(initEdgesMap)));

    // Create the rest of the dynamic graph based on the initial graph
    for (int t = 1; t < timesteps; t++) {
        // Create a single AttrGraph for this time
        std::map<int, std::unique_ptr<Vertex>> vMap;
        std::map<int, std::set<int>> edgesMap;
        for (int v = 0; v < vCount; v++) {
            // Create vertex with attribute values that have change randomly from the previous based on attrValueChange
            auto vertex = std::make_unique<Vertex>(v, attrCount);
            for (int attr = 0; attr < attrCount; attr++) {
                int attrChange = std::rand() % (2 * attrValueChange) - attrValueChange;
                int attrValue = graphMap[t - 1]->getAttrValues(v)[attr] + attrChange;
                vertex->attrValues[attr] = attrValue;
            }
            vMap[v] = std::move(vertex);

            // Just copy over the neighborhood set from the previous graph
            // TODO: make it so that the neighborhood set changes randomly based on neighborChange
            for (auto n : *graphMap[t - 1]->getNeighbors(v)) {
                edgesMap[v].insert(n);
            }
        }
        graphMap[t] = std::move(std::make_unique<AttrGraph>(std::move(vMap), std::move(edgesMap)));
    }

    std::cout << "Finish Generate" << std::endl;
}

void DyAttrGraph::printGraphs() {
    for (auto &it : graphMap) {
        std::cout << "Time: " << it.first << std::endl;
        it.second->print();
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

std::unique_ptr<TrendGraph> DyAttrGraph::createTrendAg() {
    int newAttrCount = attrCount * 3;
    std::map<int, std::unique_ptr<AttrGraph>> newGraphMap;
    std::set<int> vIdSet = DyAttrGraph::getCompleteVIdSet();
    for (int t = 1; t < timesteps; t++) {
        std::map<int, std::unique_ptr<Vertex>> vMap;
        std::map<int, std::set<int>> edgesMap;
        for (auto vId : vIdSet) {
            auto vertex = std::make_unique<Vertex>(vId, newAttrCount);
            if (graphMap[t - 1]->getVMap()->find(vId) != graphMap[t - 1]->getVMap()->end() &&
                graphMap[t]    ->getVMap()->find(vId) != graphMap[t]    ->getVMap()->end())
            {
                int *currentAttrValues = graphMap[t]->getAttrValues(vId);
                int *prevAttrValues = graphMap[t - 1]->getAttrValues(vId);
                for (int attr = 0; attr < attrCount; attr++) {
                    int change = currentAttrValues[attr] - prevAttrValues[attr];
                    int newAttrValue = 1;
                    if (change < 0) {
                        newAttrValue = 0;
                    } else if (change > 0) {
                        newAttrValue = 2;
                    }
                    vertex->attrValues[attr * 3 + newAttrValue] = 1;
                }
            } else {
                for (int attr = 0; attr < attrCount; attr++) {
                    vertex->attrValues[attr * 3 + 1] = 1;
                }
            }
            vMap[vId] = std::move(vertex);
            for (auto n: *graphMap[t]->getNeighbors(vId)) {
                edgesMap[vId].insert(n);
            }
        }

        newGraphMap[t] = std::move(std::make_unique<AttrGraph>(std::move(vMap), std::move(edgesMap)));
    }
    return std::move(std::make_unique<TrendGraph>(std::move(newGraphMap)));
}

int DyAttrGraph::getAttrCount() const {
    return attrCount;
}

int DyAttrGraph::getTimesteps() const {
    return timesteps;
}

int DyAttrGraph::calEdgeNumbers() {
    int sum = 0;
    for (auto &graph : graphMap) {
        for (auto &vIt : *graph.second->getVMap()) {
            sum += graph.second->getNeighbors(vIt.first)->size();
        }
    }
    return sum;
}

int DyAttrGraph::calVertexNumbers() {
    int sum = 0;
    for (auto &graph : graphMap) {
        sum += graph.second->getVMap()->size();
    }
    return sum;
}

void TrendGraph::plantCorePatterns(const std::vector<std::unique_ptr<CorePattern>> *patterns, float minSup, float minLift, float minConf) {
   int minSupRelative = (int) (minSup * (float) TrendGraph::calVertexNumbers());

   auto invAttrMap = std::make_unique<InvAttrMap>(TrendGraph::getAttrCount());
   invAttrMap->initFromDyAttrGraph(this);

   auto visited = std::set<std::tuple<int, int, int>>();
   for (auto &pattern : *patterns) {
       // Satisfy minSup
       int sup = 0;
       while (sup <= minSupRelative * pattern->getSize()) {
           int t = std::rand() % (TrendGraph::getTimesteps() - 2) + 2;
           int vId = std::rand() % TrendGraph::graphMap[t]->getVMap()->size();
           int attr = (int) pattern->childAttr / 3;
           auto supPoint = std::tuple<int, int, int>(t, vId, attr);
           if (visited.find(supPoint) == visited.end()) {
               auto neighbors = TrendGraph::graphMap[t - 1]->getNeighbors(vId);
               for (int pTrendAttr : pattern->parentAttrs) {
                   int pAttr = (int) pTrendAttr / 3;
                   for (int nId : *neighbors) {
                       // TODO: Satisfy lift and confidence here

                       auto nSupPoint = std::tuple<int, int, int>(t - 1, nId, pAttr);
                       if (visited.find(nSupPoint) == visited.end()) {
                           TrendGraph::setAttrValue(t - 1, nId, pTrendAttr);
                           visited.insert(nSupPoint);
                           break;
                       }
                   }
               }
               TrendGraph::setAttrValue(t, vId, pattern->childAttr);
               visited.insert(supPoint);
               sup++;
               //std::cout << "sup: " << sup << ", minsupRelative: " << minSupRelative << std::endl;
           }
       }
   }
}

void TrendGraph::setAttrValue(int t, int vId, int trendAttr) {
    int attr = (int) trendAttr / 3;
    auto attrPtr = &graphMap[t]->getAttrValues(vId)[attr * 3];
    attrPtr[0] = 0;
    attrPtr[1] = 0;
    attrPtr[2] = 0;
    attrPtr[trendAttr % 3] = 1;
}

std::set<int> DyAttrGraph::getCompleteVIdSet() {
    std::set<int> completeVIdSet;
    for(auto &graph : graphMap) {
        for(auto &vIt : *graph.second->getVMap()) {
            completeVIdSet.insert(vIt.second->getId());
        }
    }
    return completeVIdSet;
}
